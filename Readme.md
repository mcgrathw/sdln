# sdln(1)

sdln - simple data line

a simple status monitor for window managers that use **WM_NAME** or stdin to  
fill a status area.


# license

unless otherwise stated all the programs i author are licensed with the  
**ISC LICENCE** with terms as follows:

ISC License (Internet Systems Consortium)

Permission to use, copy, modify, and distribute this software for any  
 purpose with or without fee is hereby granted, provided that the above  
 copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES  
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF  
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR  
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN  
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF  
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

